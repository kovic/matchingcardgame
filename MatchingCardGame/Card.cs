﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace MatchingCardGame
{
    /// <summary>
    /// Card object on the grid
    /// </summary>
    class Card
    {
        #region Fields

        Texture2D frontCardTexture;
        Texture2D coverTexture;
        Texture2D currentTexture;
        Rectangle cardDrawRectangle;

        string cardName;
        bool isCardMatched = false;
        bool clickStarted = false;
        bool buttonReleased = true;
        bool isCardPicked = false;
        bool isfliping = false;
        bool coverSideVisible = true;
        bool frontSideVisible = false;
        bool isFlipingBack = false;

        #endregion

        #region Constructor

        public Card(ContentManager contentManager, string cardName, string coverName)
        {
            this.cardName = cardName;

            // Set card textures
            frontCardTexture = contentManager.Load<Texture2D>(cardName);
            coverTexture = contentManager.Load<Texture2D>(coverName);

            currentTexture = coverTexture;
            IsFlipingBack = false;
        }

        #endregion

        #region Properties

        public string CardName
        {
            get { return cardName; }
        }

        public bool IsCardMatched
        {
            get { return isCardMatched; }
            set { isCardMatched = value; }
        }

        public Rectangle CardDrawRectangle
        {
            get { return cardDrawRectangle; }
        }

        public bool IsCardPicked
        {
            get { return isCardPicked; }
            set { isCardPicked = value; }
        }

        public Texture2D CoverTexture
        {
            get { return coverTexture; }
        }

        public Texture2D FrontCardTexture
        {
            get { return frontCardTexture; }
        }

        public bool IsFliping
        {
            get { return isfliping; }
            set { isfliping = value; }
        }

        public bool IsFlipingBack
        {
            get { return isFlipingBack; }
            set { isFlipingBack = value; }
        }

        #endregion

        #region Methods

        // Set the DrawRectangle for the card
        public void SetCardDrawRectangle (int x, int y, int cardSize)
        {
            cardDrawRectangle = new Rectangle(x, y, cardSize, cardSize);
        }
        
        // Draw the card
        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(currentTexture, cardDrawRectangle, Color.White);
        }

        /// <summary>
        /// Flip the card to front side
        /// </summary>
        public void FlipCard()
        {
            if (coverSideVisible)
            {
                cardDrawRectangle.X += GameConstants.CARD_ANIMATION_RATE / 2;
                cardDrawRectangle.Width -= GameConstants.CARD_ANIMATION_RATE;
                if (cardDrawRectangle.Width <= 0)
                {
                    cardDrawRectangle.Width = 0;
                    coverSideVisible = false;
                }
            }
            else
            {
                if (isfliping)
                {
                    currentTexture = frontCardTexture;
                    cardDrawRectangle.X -= GameConstants.CARD_ANIMATION_RATE / 2;
                    cardDrawRectangle.Width += GameConstants.CARD_ANIMATION_RATE;
                    if (cardDrawRectangle.Width >= GameConstants.CARD_SIZE)
                    {
                        cardDrawRectangle.Width = GameConstants.CARD_SIZE;
                        frontSideVisible = true;
                        isfliping = false;
                    }
                }
            }
        }

        /// <summary>
        /// Flip card back to cover side
        /// </summary>
        public void FlipBack()
        {
            if (frontSideVisible)
            {
                cardDrawRectangle.X += GameConstants.CARD_ANIMATION_RATE / 2;
                cardDrawRectangle.Width -= GameConstants.CARD_ANIMATION_RATE;
                if (cardDrawRectangle.Width <= 0)
                {
                    cardDrawRectangle.Width = 0;
                    frontSideVisible = false;
                }
            }
            else
            {
                if (isFlipingBack)
                {
                    currentTexture = coverTexture;
                    cardDrawRectangle.X -= GameConstants.CARD_ANIMATION_RATE / 2;
                    cardDrawRectangle.Width += GameConstants.CARD_ANIMATION_RATE;
                    if (cardDrawRectangle.Width >= GameConstants.CARD_SIZE)
                    {
                        cardDrawRectangle.Width = GameConstants.CARD_SIZE;
                        coverSideVisible = true;
                        isFlipingBack = false;
                    }
                }
            }
        }



        /// <summary>
        /// Check if card was clicked
        /// </summary>
        /// <param name="mouseState">Mouse state</param>
        /// <returns></returns>
        public bool IsCardClicked (MouseState mouseState)
        {
            if (cardDrawRectangle.Contains(mouseState.X, mouseState.Y))
            {
                if (mouseState.LeftButton == ButtonState.Pressed && buttonReleased == true)
                {
                    clickStarted = true;
                    buttonReleased = false;
                }
                else if (mouseState.LeftButton == ButtonState.Released)
                {
                    buttonReleased = true;
                    if (clickStarted)
                    {
                        clickStarted = false;
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion
    }
}
