﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace MatchingCardGame
{
    /// <summary>
    /// Buttons to play another game or quit after a single game round
    /// </summary>
    class EndGameButton
    {
        #region Fields

        Texture2D buttonTexture;
        Rectangle buttonDrawRectangle;
        bool clickStarted;
        bool buttonReleased;

        #endregion

        #region Constructors

        public EndGameButton (Rectangle drawRectangle, Texture2D buttonTexture)
        {
            this.buttonTexture = buttonTexture;
            this.buttonDrawRectangle = drawRectangle;
        }

        #endregion

        #region Methods

        public void Draw (SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(buttonTexture, buttonDrawRectangle, Color.White);
        }
        
        /// <summary>
        /// Check if button was clicked
        /// </summary>
        /// <param name="mouseState">Mouse state</param>
        /// <returns></returns>
        public bool IsButtonClicked(MouseState mouseState)
        {
            if (buttonDrawRectangle.Contains(mouseState.X, mouseState.Y))
            {
                if (mouseState.LeftButton == ButtonState.Pressed && buttonReleased == true)
                {
                    clickStarted = true;
                    buttonReleased = false;
                }
                else if (mouseState.LeftButton == ButtonState.Released)
                {
                    buttonReleased = true;
                    if (clickStarted)
                    {
                        clickStarted = false;
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion
    }
}
