﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatchingCardGame
{
    /// <summary>
    /// Holds all the constants for the game
    /// </summary>
    public static class GameConstants
    {
        // Window constants
        public const int WINDOW_WIDTH = 1920;
        public const int WINDOW_HEIGHT = 1080;

        // Player constants
        public const int PLAYER_COUNT = 4;
        public const string PLAYER_1 = "Player 1";
        public const string PLAYER_2 = "Player 2";
        public const string PLAYER_3 = "Player 3";
        public const string PLAYER_4 = "Player 4";

        // Grid constants
        public const int GRID_WIDTH = CARD_SIZE * COLUMNS;
        public const int GRID_HEIGHT = CARD_SIZE * ROWS;
        public const int COLUMNS = 10; //REMARK: Product of COLUMNS and ROWS should give an EVEN number
        public const int ROWS = 5; //REMARK: Product of COLUMNS and ROWS should give an EVEN number

        public const int CARD_SIZE = 160; // REMARK: Card size should be adjusted when columns and rows are adjusted
        public const float LEFT_SIDE_PLAYER_FLIP = 4.71f;
        public const float RIGHT_SIDE_PLAYER_FLIP = 1.57f;

        // Scoreboard constants
        public const int SCOREBOARD_WIDTH = 600;
        public const int SCOREBOARD_HEIGHT = 400;
        public const int PLAYER_WIN_DISTANCE = 50;

        // Card constants
        public const int CARD_OPEN_TIME = 1500;
        public const int CARD_ANIMATION_RATE = 6;
    }
}
