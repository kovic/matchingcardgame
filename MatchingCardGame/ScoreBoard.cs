﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MatchingCardGame
{
    /// <summary>
    /// Scoreboard object to show the winners after single game round
    /// </summary>
    class ScoreBoard
    {
        #region Fields



        Texture2D scoreBoardTexture;
        Rectangle scoreDrawRectangle;
        bool canShowScore = false;
        string singleWinnerText = "Winner is: ";
        string multiWinnerText = "Winners are: ";

        #endregion

        #region Constructors

        public ScoreBoard(ContentManager contentManager, string spriteName)
        {
            scoreBoardTexture = contentManager.Load<Texture2D>(spriteName);
            scoreDrawRectangle = new Rectangle(GameConstants.WINDOW_WIDTH / 2 - GameConstants.SCOREBOARD_WIDTH / 2, GameConstants.WINDOW_HEIGHT / 2 - GameConstants.SCOREBOARD_HEIGHT / 2,
                GameConstants.SCOREBOARD_WIDTH, GameConstants.SCOREBOARD_HEIGHT);
        }

        #endregion

        #region Properties

        private Vector2 GetWinnerTagPosition
        {
            get { return new Vector2(scoreDrawRectangle.X + scoreDrawRectangle.Width / 3, scoreDrawRectangle.Y + scoreDrawRectangle.Height / 5); }
        }

        private Vector2 FirstWinnerScorePosition
        {
            get { return new Vector2(scoreDrawRectangle.X + scoreDrawRectangle.Width / 3, GetWinnerTagPosition.Y + GameConstants.PLAYER_WIN_DISTANCE); }
        }

        private Vector2 SecondWinnersScorePosition
        {
            get { return new Vector2(scoreDrawRectangle.X + scoreDrawRectangle.Width / 3, FirstWinnerScorePosition.Y + GameConstants.PLAYER_WIN_DISTANCE); }
        }

        private Vector2 ThirdWinnersScorePosition
        {
            get { return new Vector2(scoreDrawRectangle.X + scoreDrawRectangle.Width / 3, SecondWinnersScorePosition.Y + GameConstants.PLAYER_WIN_DISTANCE); }
        }

        private Vector2 FourthWinnersScorePosition
        {
            get { return new Vector2(scoreDrawRectangle.X + scoreDrawRectangle.Width / 3, ThirdWinnersScorePosition.Y + GameConstants.PLAYER_WIN_DISTANCE); }
        }

        public bool CanShowScore
        {
            get { return canShowScore; }
            set { canShowScore = value; }
        }

        public int ScoreBoardLeftEdge
        {
            get { return scoreDrawRectangle.X; }
        }

        public int ScoreBoardRightEdge
        {
            get { return scoreDrawRectangle.X + GameConstants.SCOREBOARD_WIDTH; }
        }

        public int ScoreBoardTopEdge
        {
            get { return scoreDrawRectangle.Y; }
        }

        public int ScoreBoardBottomEdge
        {
            get { return scoreDrawRectangle.Y + GameConstants.SCOREBOARD_HEIGHT; }
        }

        #endregion

        #region Methods

        private void Draw (SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(scoreBoardTexture, scoreDrawRectangle, Color.White);
        }

        private void DrawWinner (SpriteBatch spriteBatch, SpriteFont spriteFont, string player, Vector2 position)
        {
            spriteBatch.DrawString(spriteFont, player, position, Color.Black);
        }

        private void DrawWinnerTag(SpriteBatch spriteBatch, SpriteFont spriteFont, string WinnerTag, Vector2 position)
        {
            spriteBatch.DrawString(spriteFont, WinnerTag, position, Color.Black);
        }

        // Draw the scoreboard and add a list of winners
        public void DrawScoreBoard(SpriteBatch spriteBatch, SpriteFont spriteFont, List<Player> winners)
        {
            Draw(spriteBatch);

            // Posible winning cases
            switch (winners.Count)
            {
                case 1:
                    DrawWinnerTag(spriteBatch, spriteFont, singleWinnerText, GetWinnerTagPosition);
                    DrawWinner(spriteBatch, spriteFont, winners[0].PlayerName, FirstWinnerScorePosition);
                    break;
                case 2:
                    DrawWinnerTag(spriteBatch, spriteFont, multiWinnerText, GetWinnerTagPosition);
                    DrawWinner(spriteBatch, spriteFont, winners[0].PlayerName, FirstWinnerScorePosition);
                    DrawWinner(spriteBatch, spriteFont, winners[1].PlayerName, SecondWinnersScorePosition);
                    break;
                case 3:
                    DrawWinnerTag(spriteBatch, spriteFont, multiWinnerText, GetWinnerTagPosition);
                    DrawWinner(spriteBatch, spriteFont, winners[0].PlayerName, FirstWinnerScorePosition);
                    DrawWinner(spriteBatch, spriteFont, winners[1].PlayerName, SecondWinnersScorePosition);
                    DrawWinner(spriteBatch, spriteFont, winners[2].PlayerName, ThirdWinnersScorePosition);
                    break;
                case 4:
                    DrawWinnerTag(spriteBatch, spriteFont, multiWinnerText, GetWinnerTagPosition);
                    DrawWinner(spriteBatch, spriteFont, winners[0].PlayerName, FirstWinnerScorePosition);
                    DrawWinner(spriteBatch, spriteFont, winners[1].PlayerName, SecondWinnersScorePosition);
                    DrawWinner(spriteBatch, spriteFont, winners[2].PlayerName, ThirdWinnersScorePosition);
                    DrawWinner(spriteBatch, spriteFont, winners[3].PlayerName, FourthWinnersScorePosition);
                    break;
            }
        }

        /// <summary>
        /// Calculate the winner among player. If more player share the best score, they are all winners.
        /// </summary>
        /// <param name="players">Array of players</param>
        /// <param name="winners">List which will store the winners</param>
        /// <returns></returns>
        public List<Player> CalculateWinner (Player[] players, List<Player> winners)
        {
            // Sort the players by score in descending order
            for (int i = 0; i < players.Length; i++)
            {
                for (int j = i + 1; j < players.Length; j++)
                {
                    if (players[i].Score < players[j].Score)
                    {
                        Player temp;

                        temp = players[i];
                        players[i] = players[j];
                        players[j] = temp;
                    }
                }
            }

            int bestScore = players[0].Score;
            
            // Check if more than one player has the best score
            foreach (Player player in players)
                if (player.Score == bestScore)
                    winners.Add(player);

            return winners;
        }

        #endregion
    }
}
