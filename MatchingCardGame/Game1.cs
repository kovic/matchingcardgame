﻿using System.Collections.Generic;
using System.IO;
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MatchingCardGame
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // grid support
        Card[,] gridArray;
        Grid grid;

        // cards support
        List<string> listOfFileNames;
        Card firstCardInPair;
        Card secondCardInPair;

        int elapsedTime = 0;
        bool cardsFliped = false;

        // player support
        Player[] players; 
        List<Player> winners;
        List<string> playerNames;
        Player currentPlayer;
        int startingPlayerIndex = 0;

        bool playersAdded = false;

        // score support
        ScoreBoard scoreBoard;
        int numberOfMatchedCards = 0;
        bool isGameWon = false;

        // writting support
        SpriteFont spriteFont;

        // button support
        EndGameButton playAgainButton;
        EndGameButton quitButton;
        Texture2D playAgainButtonTexture;
        Texture2D quitButtonTexture;
        int buttonOffset = 100;
        bool askToPlayAgain;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // Window size
            graphics.PreferredBackBufferWidth = GameConstants.WINDOW_WIDTH;
            graphics.PreferredBackBufferHeight = GameConstants.WINDOW_HEIGHT;

            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            gridArray = new Card[GameConstants.COLUMNS, GameConstants.ROWS];

            // Grad file adresses from all the picture assets in a form of string
            String[] files = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\Content\graphics\cardsBack");

            // Support fpr fetching card names
            listOfFileNames = new List<string>();
            foreach (string file in files)
                listOfFileNames.Add(Path.GetFileNameWithoutExtension(file));

            // Player fields initialization
            players = new Player[GameConstants.PLAYER_COUNT];
            playerNames = new List<string>() { GameConstants.PLAYER_1, GameConstants.PLAYER_2, GameConstants.PLAYER_3, GameConstants.PLAYER_4};
            winners = new List<Player>();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Load font for all written content
            spriteFont = Content.Load<SpriteFont>(@"fonts\Arial24");

            // Initialize grid
            grid = new Grid(Content, @"graphics\Grid", gridArray, listOfFileNames);

            // Initialize scoreboard
            scoreBoard = new ScoreBoard(Content, @"graphics\ScoreBoard");

            // Initialize buttons
            playAgainButtonTexture = Content.Load<Texture2D>(@"graphics\AgainButton");
            quitButtonTexture = Content.Load<Texture2D>(@"graphics\QuitButton");
            playAgainButton = new EndGameButton(new Rectangle(scoreBoard.ScoreBoardLeftEdge - playAgainButtonTexture.Width - buttonOffset,
                scoreBoard.ScoreBoardBottomEdge - playAgainButtonTexture.Height, playAgainButtonTexture.Width, playAgainButtonTexture.Height), 
                playAgainButtonTexture);
            quitButton = new EndGameButton(new Rectangle(scoreBoard.ScoreBoardRightEdge + buttonOffset,
                scoreBoard.ScoreBoardBottomEdge - quitButtonTexture.Height, quitButtonTexture.Width, quitButtonTexture.Height),
                quitButtonTexture);
                
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            MouseState mouseState = Mouse.GetState();

            // Check if players are added and if not, add them
            if(!playersAdded)
            {         
                switch (players.Length)
                {
                    case 1:
                        players[0] = new Player(Content, Grid.TopPlayerPosition, playerNames[0]);
                        break;
                    case 2:
                        players[0] = new Player(Content, Grid.TopPlayerPosition, playerNames[0]);
                        players[1] = new Player(Content, Grid.BottomPlayerPosition, playerNames[1]);
                        break;
                    case 3:
                        players[0] = new Player(Content, Grid.TopPlayerPosition, playerNames[0]);
                        players[1] = new Player(Content, Grid.LeftPlayerPosition, playerNames[1]);
                        players[2] = new Player(Content, Grid.BottomPlayerPosition, playerNames[2]);
                        break;
                    case 4:
                        players[0] = new Player(Content, Grid.TopPlayerPosition, playerNames[0]);
                        players[1] = new Player(Content, Grid.LeftPlayerPosition, playerNames[1]);
                        players[2] = new Player(Content, Grid.BottomPlayerPosition, playerNames[2]);
                        players[3] = new Player(Content, Grid.RightRightPosition, playerNames[3]);
                        break;
                }

                currentPlayer = players[startingPlayerIndex];

                playersAdded = true;
            }
            
            // Game logic
            if (!isGameWon)
            {
                // Continue to play if players are added and number of matched cards are less than total cards on the grid    
                if (numberOfMatchedCards < gridArray.Length && playersAdded)
                {
                    if (firstCardInPair == null)
                    {
                        // Track if the first card out of two was clicked
                        foreach (Card card in gridArray)
                        {
                            if (card.IsCardClicked(mouseState) &&
                                    card.IsCardMatched == false &&
                                        !card.IsCardPicked)
                            {
                                card.IsCardPicked = true;
                                firstCardInPair = card;
                                firstCardInPair.IsFliping = true;
                            }
                        }
                    }
                    else
                    {
                        firstCardInPair.FlipCard();
                    }

                    if (secondCardInPair == null)
                    {
                        // Track if the second card out of two was clicked
                        foreach (Card card in gridArray)
                        {
                            if (card.IsCardClicked(mouseState) &&
                                    !card.IsCardPicked &&
                                        card.IsCardMatched == false)
                            {
                                card.IsCardPicked = true;
                                secondCardInPair = card;
                                cardsFliped = true;
                                secondCardInPair.IsFliping = true;
                            }
                        }
                    }
                    else
                    {
                        secondCardInPair.FlipCard();
                    }

                    // If both cards were picked, compare them
                    if (firstCardInPair != null && secondCardInPair != null)
                    {
                        if (cardsFliped)
                        {
                            // Hold cards open for a short period of time
                            HoldCardsOpened(gameTime);
                            firstCardInPair.IsFlipingBack = true;
                            secondCardInPair.IsFlipingBack = true;
                        }
                        else
                        {
                            // Support loop for animation logic
                            if (firstCardInPair.CardName != secondCardInPair.CardName)
                            {
                                firstCardInPair.FlipBack();
                                secondCardInPair.FlipBack();
                            }
                            else
                            {
                                firstCardInPair.IsFlipingBack = false;
                                secondCardInPair.IsFlipingBack = false;
                            }

                            if (!firstCardInPair.IsFlipingBack && !secondCardInPair.IsFlipingBack)
                            {
                                // Mark cards as matched and update score
                                if (firstCardInPair.CardName == secondCardInPair.CardName)
                                {
                                    firstCardInPair.IsCardMatched = true;
                                    secondCardInPair.IsCardMatched = true;

                                    numberOfMatchedCards += 2;
                                    currentPlayer.UpdateScore();
                                }
                                // Unpick cards unpaired cards and change player
                                else
                                {
                                    firstCardInPair.IsCardPicked = false;
                                    secondCardInPair.IsCardPicked = false;
                                    currentPlayer = currentPlayer.ChangePlayer(players);
                                }
                                // Reset the place holders for opened cards
                                firstCardInPair = null;
                                secondCardInPair = null;
                            }
                        }
                    }
                }
            }
            // Game is won, show score and ask to play again
            else
            {
                if (!scoreBoard.CanShowScore)
                {
                    winners = scoreBoard.CalculateWinner(players, winners);
                    scoreBoard.CanShowScore = true;
                    askToPlayAgain = true;
                }

                AskToPlayAgain(mouseState);
            }

            // Check if game is won after each update
            CheckIfGameWon();

            base.Update(gameTime);

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            // Draw the grid background
            grid.Draw(spriteBatch);

            // Draw the cards
            foreach (Card card in gridArray)
            {
                if (!card.IsCardMatched)
                    card.Draw(spriteBatch);
            }

            // Draw players
            if (playersAdded)
            {
                foreach (Player player in players)
                {
                    // Mark current player
                    if (player != currentPlayer)
                    {
                        if (player.PositiOnGrid == Grid.BottomPlayerPosition || player.PositiOnGrid == Grid.TopPlayerPosition)
                            player.Draw(spriteBatch, spriteFont, Color.White);
                        else if (player.PositiOnGrid == Grid.LeftPlayerPosition)
                            player.Draw(spriteBatch, spriteFont, Color.White, GameConstants.LEFT_SIDE_PLAYER_FLIP, Vector2.Zero);
                        else if (player.PositiOnGrid == Grid.RightRightPosition)
                            player.Draw(spriteBatch, spriteFont, Color.White, GameConstants.RIGHT_SIDE_PLAYER_FLIP, Vector2.Zero);
                    }
                    else
                    {
                        if (player.PositiOnGrid == Grid.BottomPlayerPosition || player.PositiOnGrid == Grid.TopPlayerPosition)
                            player.Draw(spriteBatch, spriteFont, Color.Red);
                        else if (player.PositiOnGrid == Grid.LeftPlayerPosition)
                            player.Draw(spriteBatch, spriteFont, Color.Red, GameConstants.LEFT_SIDE_PLAYER_FLIP, Vector2.Zero);
                        else if (player.PositiOnGrid == Grid.RightRightPosition)
                            player.Draw(spriteBatch, spriteFont, Color.Red, GameConstants.RIGHT_SIDE_PLAYER_FLIP, Vector2.Zero);
                    }
                }
            }

            // Draw scoreboard
            if (scoreBoard.CanShowScore)
            {
                scoreBoard.DrawScoreBoard(spriteBatch, spriteFont, winners);
            }

            // Draw buttons
            if (askToPlayAgain)
            {
                playAgainButton.Draw(spriteBatch);
                quitButton.Draw(spriteBatch);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Hold cards opened for a time specefied in miliseconds
        /// </summary>
        /// <param name="gameTime">Time since last update</param>
        private void HoldCardsOpened (GameTime gameTime)
        {
            elapsedTime += gameTime.ElapsedGameTime.Milliseconds;
            if (elapsedTime >= GameConstants.CARD_OPEN_TIME)
            {
                cardsFliped = false;
                elapsedTime = 0;
            }
        }

        private void CheckIfGameWon ()
        {
            if (numberOfMatchedCards == gridArray.Length)
                isGameWon = true;
            else
                isGameWon = false;
        }

        private void AskToPlayAgain(MouseState mouseState)
        {
            // Reset neccessary fields for new round
            if (playAgainButton.IsButtonClicked(mouseState))
            {
                grid = new Grid(Content, @"graphics\Grid", gridArray, listOfFileNames);
                currentPlayer = null;
                numberOfMatchedCards = 0;
                winners.Clear();
                playersAdded = false;
                isGameWon = false;
                scoreBoard.CanShowScore = false;
                askToPlayAgain = false;
            }

            // Exit game
            if (quitButton.IsButtonClicked(mouseState))
                Exit();
        }
    }
}
