﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace MatchingCardGame
{
    class Player
    {
        #region Fields

        Vector2 playerNamePosition;
        string playerName;
        string scoreString;
        Vector2 scorePosition;
        int scorePositionOffset = 40;
        int score = 0;

        #endregion

        #region Constructors

        public Player (ContentManager contentManager, Vector2 playerNamePosition, string playerName)
        {
            this.playerNamePosition = playerNamePosition;
            this.playerName = playerName;

            SetPlayerScoreField();
        }

        #endregion 

        #region Properties

        public string PlayerName
        {
            get { return playerName; }
            set { playerName = value; }
        }

        public Vector2 PositiOnGrid
        {
            get { return playerNamePosition; }
        }

        public int Score
        {
            get { return score; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Change the current player
        /// </summary>
        /// <param name="players">List of active players</param>
        /// <param name="currentPlayer">Player who's turn had just ended</param>
        /// <returns></returns>
        public Player ChangePlayer(Player[] players)
        {
            int nextPlayer = Array.IndexOf(players, this) + 1;

            // check if index of next player is out of bounds and if so, reset the index
            if (nextPlayer >= players.Length)
                nextPlayer = 0;

            return players[nextPlayer];
        }

        /// <summary>
        /// Set the layout of the socre field after a single game round
        /// </summary>
        private void SetPlayerScoreField ()
        {
            scoreString = "(Score: " + score.ToString() + " pairs)";

            if (this.PositiOnGrid == Grid.BottomPlayerPosition || this.PositiOnGrid == Grid.TopPlayerPosition)
                scorePosition = new Vector2(GameConstants.WINDOW_WIDTH / 2, playerNamePosition.Y + scorePositionOffset);
            else if (this.PositiOnGrid == Grid.LeftPlayerPosition)
                scorePosition = new Vector2(playerNamePosition.X + scorePositionOffset, GameConstants.WINDOW_HEIGHT / 2);
            else if (this.PositiOnGrid == Grid.RightRightPosition)
                scorePosition = new Vector2(playerNamePosition.X - scorePositionOffset, GameConstants.WINDOW_HEIGHT / 2);                 
        }

        public void UpdateScore ()
        {
            score += 1;
            scoreString = "(Score: " + score.ToString() + " pairs)";
        }

        // Draw player name and implement rotation
        public void Draw(SpriteBatch spritebatch, SpriteFont spriteFont, Color color, float rotation, Vector2 origin)
        {
            spritebatch.DrawString(spriteFont, playerName, playerNamePosition, color, rotation, origin, 1f, SpriteEffects.None, 0f);
            spritebatch.DrawString(spriteFont, scoreString, scorePosition, color, rotation, origin, 1f, SpriteEffects.None, 0f);
        }

        // Draw player name
        public void Draw(SpriteBatch spriteBatch, SpriteFont spriteFont, Color color)
        {
            spriteBatch.DrawString(spriteFont, playerName, playerNamePosition, color);
            spriteBatch.DrawString(spriteFont, scoreString, scorePosition, color);
        }
        
        #endregion

    }
}
