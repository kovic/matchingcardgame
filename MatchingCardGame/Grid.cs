﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace MatchingCardGame
{
    /// <summary>
    /// Grid on which the game will be played
    /// </summary>
    class Grid
    {
        #region Fields

        Texture2D texture;
        Rectangle gridDrawRectangle;
        int cardSize;
                
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for the grid that will hold the cards
        /// </summary>
        /// <param name="contentManager">Content manager</param>
        /// <param name="spriteName">Name of the texture for the grid background</param>
        /// <param name="gridArray">Two dimentional array that will hold the cards</param>
        /// <param name="listOfFileNames">List of file (texture) names for the cards</param>
        public Grid(ContentManager contentManager, string spriteName, Card[,] gridArray, List<string> listOfFileNames)
        {
            gridDrawRectangle = new Rectangle((GameConstants.WINDOW_WIDTH / 2) - GameConstants.GRID_WIDTH / 2, GameConstants.WINDOW_HEIGHT / 2 - GameConstants.GRID_HEIGHT / 2,
                GameConstants.GRID_WIDTH, GameConstants.GRID_HEIGHT);

            texture = contentManager.Load<Texture2D>(spriteName);

            SetUpGridArray(contentManager, gridArray, listOfFileNames);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Returns card size based on number of columns and rows
        /// </summary>
        public int CardSize
        {
            get
            {
                if (GameConstants.ROWS >= 4)
                    return GameConstants.GRID_HEIGHT / GameConstants.ROWS;
                else if (GameConstants.COLUMNS >= 9)
                    return GameConstants.GRID_WIDTH / GameConstants.COLUMNS;
                else
                    return GameConstants.CARD_SIZE;
            }
        }

        #endregion

        #region Static properties

        public static Vector2 BottomPlayerPosition
        {
            get { return new Vector2(GameConstants.WINDOW_WIDTH / 2, GameConstants.WINDOW_HEIGHT - (GameConstants.WINDOW_HEIGHT - GameConstants.GRID_HEIGHT) / 3); }
        }

        public static Vector2 TopPlayerPosition
        {
            get { return new Vector2(GameConstants.WINDOW_WIDTH / 2, (GameConstants.WINDOW_HEIGHT - GameConstants.GRID_HEIGHT) / 8); }
        }

        public static Vector2 LeftPlayerPosition
        {
            get { return new Vector2((GameConstants.WINDOW_WIDTH - GameConstants.GRID_WIDTH) / 10, GameConstants.WINDOW_HEIGHT / 2); }
        }

        public static Vector2 RightRightPosition
        {
            get { return new Vector2(GameConstants.WINDOW_WIDTH - (GameConstants.WINDOW_WIDTH - GameConstants.GRID_WIDTH) / 8, GameConstants.WINDOW_HEIGHT / 2); }
        }

        #endregion

        #region Methods

        // Draw the grid background
        public void Draw (SpriteBatch spritebatch)
        {
            spritebatch.Draw(texture, gridDrawRectangle, Color.White);            
        }

        // Support for centering playground on the grid
        public int OffsettForX (int cardSize)
        {
            return gridDrawRectangle.X + ((gridDrawRectangle.Width - cardSize * GameConstants.COLUMNS) / 2); 
        }

        // Support for centering playground on the grid
        public int OffsetForY (int cardSize)
        {
            return gridDrawRectangle.Y + ((gridDrawRectangle.Height - cardSize * GameConstants.ROWS) / 2); 
        }

        /// <summary>
        /// Set up the grid array for the game
        /// </summary>
        /// <param name="contentManager">Content manager</param>
        /// <param name="gridArray">Two dimentional array that will hold the cards</param>
        /// <param name="listOfFileNames">List of file (texture) names for the cards</param>
        private void SetUpGridArray(ContentManager contentManager, Card[,] gridArray, List<string> listOfFileNames)
        {
            int numberOfPairsOnTheGrid;

            // Check if there is enough files for the grid to be filled
            // REMARK: If gridArray exceeds total number of files, files will be duplicated
            if (GameConstants.COLUMNS * GameConstants.ROWS <= listOfFileNames.Count * 2)
                numberOfPairsOnTheGrid = GameConstants.COLUMNS * GameConstants.ROWS / 2;
            else
                numberOfPairsOnTheGrid = listOfFileNames.Count;

            // Fill the 2 dimensional grid with cards
            for (int i = 0; i < GameConstants.ROWS; i++)
            {
                for (int j = 0; j < GameConstants.COLUMNS; j++)
                {
                    numberOfPairsOnTheGrid--;

                    // Reset the field so that the second card of the pair is put into the grid
                    if (numberOfPairsOnTheGrid < 0)
                    {
                        if (GameConstants.COLUMNS * GameConstants.ROWS <= listOfFileNames.Count * 2)
                            numberOfPairsOnTheGrid = GameConstants.COLUMNS * GameConstants.ROWS / 2;
                        else
                            numberOfPairsOnTheGrid = listOfFileNames.Count;

                        numberOfPairsOnTheGrid--;
                    }

                    // Put a single card in the grid array
                    gridArray[j, i] = new Card(contentManager, @"graphics\cardsBack\" + listOfFileNames[numberOfPairsOnTheGrid], 
                        @"graphics\CardsFront"); 
                }
            }

            cardSize = CardSize;
            Shuffle(gridArray);

            // Add draw rectangle for each card in the grid
            for (int i = 0; i < GameConstants.COLUMNS; i++)
            {
                for (int j = 0; j < GameConstants.ROWS; j++)
                {
                    // Set the drawRectangle for a single card
                    gridArray[i, j].SetCardDrawRectangle(OffsettForX(cardSize) + i * cardSize, 
                        OffsetForY(cardSize) + j * cardSize, cardSize); 
                }
            }

        }
        
        /// <summary>
        /// Get the column in which n-th item of the array is stored
        /// </summary>
        /// <param name="n">Ordinal number of the item in an array</param>
        /// <returns></returns>
        private int GetY(int n)
        {
            return n / GameConstants.COLUMNS;
        }

        /// <summary>
        /// Get the row in which n-th item of the array is stored
        /// </summary>
        /// <param name="n">Ordinal number of the item in an array</param>
        /// <returns></returns>
        private int GetX(int n)
        {
            return n % GameConstants.COLUMNS;
        }

        /// <summary>
        /// Shuffle the items in an two-dimentional array
        /// </summary>
        /// <typeparam name="T">Type to shuffle</typeparam>
        /// <param name="array">Array of items upon the shuffle is to be conducted</param>
        private void Shuffle<T>(T[,] array)
        {
            Random rand = new Random();

            int n = array.Length;

            while (n > 1)
            {
                n--;
                int i = rand.Next(n + 1);

                T temp = array[GetX(i), GetY(i)];
                array[GetX(i), GetY(i)] = array[GetX(n), GetY(n)];
                array[GetX(n), GetY(n)] = temp;
            }
        }

        #endregion
    }
}
